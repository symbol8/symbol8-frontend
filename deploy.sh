#!/bin/bash

HOST='aws'
DEPLOY_PATH='~/docker/symbol8/'
echo "------------------------------"
echo "   DEPLOY ELM APP TO SERVER   "
echo "------------------------------"
echo "Compiling the app"
elm-app build
echo "Copying the files to the server"

scp -rF ~/.ssh/config ./build/* $HOST:$DEPLOY_PATH
