#!/bin/bash

SESSION_NAME='symbol8-elm'

tmux has-session -t $SESSION_NAME &> /dev/null

if [ $? != 0 ]
then
    tmux new-session -s $SESSION_NAME -d -n 'vim'
    tmux new-window -t $SESSION_NAME:2 -n 'git'
    tmux new-window -t $SESSION_NAME:3 -n 'server'

    tmux send-keys -t $SESSION_NAME:1 'vi' 'C-m'
    tmux send-keys -t $SESSION_NAME:3 'elm-app start' 'C-m'
    tmux split-window -h -t $SESSION_NAME:3
    tmux send-keys -t $SESSION_NAME:3.2 'yarn run watch-css' 'C-m'
    tmux split-window -v -t $SESSION_NAME:3
    tmux send-keys -t $SESSION_NAME:3.3 'elm-doc-preview' 'C-m'

    tmux select-window -t $SESSION_NAME:1
fi

tmux attach -t $SESSION_NAME
