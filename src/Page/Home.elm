module Page.Home exposing (view)

import Bootstrap.Button as Button
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Grid.Row as Row
import Bootstrap.Text as Text
import Bootstrap.Utilities.Size as Size
import Html exposing (..)
import Html.Attributes exposing (class, classList, href, src, style)
import Logo exposing (Logo)
import Route



-- VIEW


view : Logo -> { title : String, content : Html msg }
view logo =
    { title = ""
    , content = showView logo
    }


showView : Logo -> Html msg
showView logo =
    Grid.container
        [ Size.h100
        , class "d-flex flex-column"
        ]
        [ logoView logo
        , contentView
        ]


logoView : Logo -> Html msg
logoView logo =
    Grid.row
        [ Row.middleXs
        , Row.centerXs
        , Row.attrs [ class "flex-grow-1" ]
        ]
        [ Grid.col
            [ Col.xs5
            , Col.lg4
            , Col.xl3
            ]
            [ img
                [ Logo.src logo
                , class "img-fluid"
                ]
                []
            ]
        ]


contentView : Html msg
contentView =
    Grid.row
        [ Row.centerXs
        , Row.attrs [ class "flex-grow-1" ]
        ]
        [ symbolHeader
        , symbolMantra
        , buttonPhilosophy
        ]


symbolHeader : Grid.Column msg
symbolHeader =
    Grid.col
        [ Col.xs10
        , Col.textAlign Text.alignXsCenter
        ]
        [ h1 [ class "font-weight-bold" ] [ text "Symbol8" ] ]


symbolMantra : Grid.Column msg
symbolMantra =
    Grid.col
        [ Col.xs10
        , Col.textAlign Text.alignXsCenter
        ]
        [ h3 [] [ text "Everything is about" ]
        , h3 [] [ text "Points of" ]
        ]


buttonPhilosophy : Grid.Column msg
buttonPhilosophy =
    Grid.col
        [ Col.xs10
        , Col.textAlign Text.alignXsCenter
        ]
        [ Button.linkButton
            [ Button.primary
            , Button.attrs [ Route.href Route.Philosophy ]
            ]
            [ text "View" ]
        ]
