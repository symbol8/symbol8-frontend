module Page.Post exposing (Model, Msg, init, subscriptions, toSession, update, updateSessionWith, view)

import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Grid.Row as Row
import Bootstrap.Utilities.Size as Size
import Html exposing (..)
import Html.Attributes exposing (class)
import Route exposing (PostPage(..))
import Session exposing (Session)
import Task
import Time



-- MODEL


type Model
    = Post Settings


type alias Settings =
    { postPage : Route.PostPage
    , session : Session
    , timeZone : Time.Zone
    , time : Time.Posix
    , deadline : Time.Posix
    }



-- ACCESSORS


toSession : Model -> Session
toSession (Post model) =
    model.session


updateSessionWith : Model -> Session -> Model
updateSessionWith (Post model) session =
    Post { model | session = session }



-- INIT


init : Session -> PostPage -> ( Model, Cmd Msg )
init session postPage =
    let
        deadlineInMillis =
            1559974088000

        cmds =
            Cmd.batch
                [ Task.perform AdjustTimeZone Time.here
                , Task.perform Tick Time.now
                ]
    in
    ( Post
        { session = session
        , postPage = postPage
        , timeZone = Time.utc
        , time = Time.millisToPosix 0
        , deadline = Time.millisToPosix deadlineInMillis
        }
    , cmds
    )



-- UPDATE


type Msg
    = Tick Time.Posix
    | AdjustTimeZone Time.Zone


update : Msg -> Model -> ( Model, Cmd Msg )
update msg (Post model) =
    case msg of
        Tick newTime ->
            ( Post { model | time = newTime }
            , Cmd.none
            )

        AdjustTimeZone timeZone ->
            ( Post { model | timeZone = timeZone }
            , Cmd.none
            )



-- VIEW


view : Model -> { title : String, content : Html Msg }
view (Post model) =
    { title = "Posts"
    , content = countDown model
    }


countDown : Settings -> Html Msg
countDown model =
    let
        difference =
            Time.posixToMillis model.deadline
                - Time.posixToMillis model.time
                |> Time.millisToPosix

        to timeUnit =
            difference
                |> timeUnit model.timeZone
                |> String.fromInt

        days =
            to Time.toDay

        hours =
            to Time.toHour

        minutes =
            to Time.toMinute

        seconds =
            to Time.toSecond
    in
    Grid.container [ Size.h100 ]
        [ h1 [ class "display-4 text-center mb-5" ] [ text "Next release in" ]
        , unitView days "days"
        , unitView hours "hours"
        , unitView minutes "minutes"
        , unitView seconds "seconds"
        ]


unitView : String -> String -> Html msg
unitView days timeUnit =
    Grid.row
        [ Row.middleXs
        , Row.centerXs
        , Row.attrs [ class "text-center" ]
        ]
        [ Grid.col
            [ Col.xs12
            ]
            [ h1 [] [ text days ] ]
        , Grid.col
            [ Col.xs12
            ]
            [ h4 [] [ text timeUnit ] ]
        ]



-- SUBSCRIPTION


subscriptions : Model -> Sub Msg
subscriptions _ =
    Time.every 1000 Tick
