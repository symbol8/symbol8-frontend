module Page.NotFound exposing (view)

import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Grid.Row as Row
import Bootstrap.Text as Text
import Bootstrap.Utilities.Size as Size
import Browser
import Html exposing (..)
import Html.Attributes exposing (style)


view : { title : String, content : Html msg }
view =
    { title = "Not found"
    , content =
        Grid.container [ Size.h100 ]
            [ notFoundView ]
    }


notFoundView : Html msg
notFoundView =
    Grid.row
        [ Row.middleXs
        , Row.centerXs
        , Row.attrs [ Size.h100 ]
        ]
        [ Grid.col
            [ Col.xs12
            , Col.textAlign Text.alignXsCenter
            ]
            [ h1 [] [ text "Sorry" ]
            ]
        , Grid.col
            [ Col.xs12
            , Col.textAlign Text.alignXsCenter
            ]
            [ h3 [] [ text "This page was not found" ] ]
        ]
