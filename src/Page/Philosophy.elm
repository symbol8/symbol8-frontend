module Page.Philosophy exposing (view)

import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Grid.Row as Row
import Bootstrap.Text as Text
import Bootstrap.Utilities.Size as Size
import Bootstrap.Utilities.Spacing as Spacing
import Html exposing (..)
import Html.Attributes exposing (class, href, style)



-- VIEW


view : { title : String, content : Html msg }
view =
    { title = ""
    , content =
        Grid.container
            [ Size.h100
            , class "text-center"
            ]
            [ idelogyView
            , whyView
            ]
    }


whyView : Html msg
whyView =
    Grid.row
        []
        [ whyRail
        , whyContent
        ]


idelogyView : Html msg
idelogyView =
    Grid.row
        [ Row.attrs [ Spacing.mt4 ] ]
        [ ideologyRail
        , ideologyContent
        ]



-- RAILS
-- TODO: ICON QUESTION


whyRail : Grid.Column msg
whyRail =
    Grid.col
        [ Col.xs12
        , Col.md2
        , Col.textAlign Text.alignXsCenter
        ]
        [ icon "fa-question-circle" ]


ideologyRail : Grid.Column msg
ideologyRail =
    Grid.col
        [ Col.xs12
        , Col.md2
        , Col.textAlign Text.alignXsCenter
        ]
        [ icon "fa-lightbulb-o" ]



-- CONTENT


whyContent : Grid.Column msg
whyContent =
    Grid.col
        [ Col.offsetMd1
        , Col.xs
        , Col.textAlign Text.alignMdLeft
        ]
        [ h3 [ class "font-weight-bold" ] [ text "WHY" ]
        , h5 [ Spacing.mt2 ]
            [ text "Symbol8 has not a"
            , i [] [ text " why" ]
            , text ". It has always existed. It's like the air, we don't notice it until we know it. When the time arrives, we will discover infinite symbols, everyone with infinite perceptions, meanings, and feelings. It depends on how we want to live"
            , purpleBold " THAT "
            , text "moment. The"
            , purpleBold " REAL "
            , text "answer to the"
            , i [] [ text " WHY " ]
            , text "? Don't ask it, just"
            , purpleBold " FEEL "
            , text "it."
            ]
        ]


ideologyContent : Grid.Column msg
ideologyContent =
    Grid.col
        [ Col.offsetMd1
        , Col.xs
        , Col.textAlign Text.alignMdLeft
        ]
        [ h3 [ class "font-weight-bold" ] [ text "IDEOLOGY" ]
        , h5 [ Spacing.mt2 ]
            [ text "Symbol8 is a movement that encloses all the symbols of every universe. All the time you notice its soul, it has a different shape or dimension: maybe it's the"
            , purpleBold " 8 "
            , text "in the time, on a plate, or even the shape of a rubber band. It's the realization of something bigger, which I call"
            , purpleBold " SET"
            , text ". Until you don't give any particular attention to the object, you confuse it in the background. Suddenly, when you turn back and you notice that object again, you are invaded by an explosion of emotions, which are the realization of THAT Symbol. Only an INFINITE could contain all the Symbols."
            ]
        ]


purpleBold : String -> Html msg
purpleBold content =
    b [ class "text-primary" ] [ text content ]


icon : String -> Html msg
icon kind =
    i
        [ class <| "rounded fa " ++ kind
        , style "font-size" "30px"
        ]
        []
