module Page.Header exposing (Model, Msg, Page(..), init, subscriptions, toLogo, update, view)

import Bootstrap.Navbar as Navbar
import Html exposing (Attribute, Html, a, img, text)
import Html.Attributes exposing (class, href, style)
import Logo exposing (Logo)
import Route exposing (Route)



-- MODEL


type Model
    = Model Settings


type alias Settings =
    { logo : Logo
    , navState : Navbar.State
    }


type Page
    = Other
    | Home
    | Show
    | Philosophy
    | Posts
    | Login



-- HELPERS


toLogo : Model -> Logo
toLogo (Model settings) =
    settings.logo



-- INIT


init : Logo -> ( Model, Cmd Msg )
init logo =
    let
        ( navState, navCmd ) =
            Navbar.initialState NavbarMsg

        navSettings =
            { logo = logo
            , navState = navState
            }
    in
    ( Model navSettings, navCmd )



-- VIEW


view : Model -> Page -> Html Msg
view (Model settings) page =
    let
        items =
            navbarItems page

        active =
            \customRoute ->
                isActive page customRoute
    in
    Navbar.config NavbarMsg
        |> Navbar.fixTop
        |> logoBrand settings.logo
        |> Navbar.items
            (items
                ++ postsDropdown (active (Route.Post Route.AllPosts))
            )
        |> Navbar.customItems [ maybeViewer (active Route.Login) ]
        |> Navbar.view settings.navState


logoBrand : Logo -> (Navbar.Config msg -> Navbar.Config msg)
logoBrand logo =
    Navbar.brand
        [ Route.href Route.Home ]
        [ img
            [ Logo.src logo
            , style "width" "33px"
            ]
            []
        ]


maybeViewer : Bool -> Navbar.CustomItem msg
maybeViewer active =
    let
        buttonClass =
            class
                ("btn btn-"
                    ++ (if active then
                            "primary"

                        else
                            "outline-primary"
                       )
                )
    in
    Navbar.customItem
        (a
            [ buttonClass
            , Route.href Route.Login
            ]
            [ text "Login" ]
        )


navbarItems : Page -> List (Navbar.Item Msg)
navbarItems page =
    let
        linkTo =
            \route ->
                navbarLink page route [ Route.href route ]
    in
    [ linkTo Route.Home [ text "Home" ]
    , linkTo Route.Philosophy [ text "Philosophy" ]
    ]


navbarLink : Page -> Route -> List (Attribute Msg) -> List (Html Msg) -> Navbar.Item Msg
navbarLink page route attrs linkContent =
    let
        linkType =
            \itemType -> itemType attrs linkContent
    in
    if isActive page route then
        linkType Navbar.itemLinkActive

    else
        linkType Navbar.itemLink


isActive : Page -> Route -> Bool
isActive page route =
    case ( page, route ) of
        ( Home, Route.Home ) ->
            True

        ( Philosophy, Route.Philosophy ) ->
            True

        ( Posts, Route.Post _ ) ->
            True

        ( Login, Route.Login ) ->
            True

        _ ->
            False


postsDropdown : Bool -> List (Navbar.Item msg)
postsDropdown active =
    let
        maybeActive =
            let
                ref =
                    [ href "" ]
            in
            if active then
                class "active" :: ref

            else
                ref

        dropItemLinkTo =
            \postPage linkContent ->
                let
                    route =
                        Route.Post postPage
                in
                Navbar.dropdownItem [ Route.href route ] [ text linkContent ]
    in
    [ Navbar.dropdown
        { id = "nav-posts-dropdown"
        , toggle =
            Navbar.dropdownToggle
                maybeActive
                [ text "Posts" ]
        , items =
            [ dropItemLinkTo Route.AllPosts "All posts"
            , dropItemLinkTo Route.NewPost "New post"
            ]
        }
    ]



-- UPDATE


type Msg
    = NavbarMsg Navbar.State


update : Msg -> Model -> ( Model, Cmd Msg )
update msg (Model settings) =
    case msg of
        NavbarMsg newState ->
            ( Model { settings | navState = newState }, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions (Model settings) =
    Navbar.subscriptions settings.navState NavbarMsg
