module Page.Login exposing (Model, Msg, init, subscriptions, toSession, update, updateSessionWith, view)

import Api exposing (Auth)
import Bootstrap.Button as Button
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Bootstrap.Form as Form
import Bootstrap.Form.Input as Input
import Bootstrap.General.HAlign as Align
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Grid.Row as Row
import Bootstrap.Text as Text
import Bootstrap.Utilities.Size as Size
import Bootstrap.Utilities.Spacing as Spacing
import FormField exposing (Constraint(..), FormField(..), FormType, withConstraints, withLabel, withType)
import Html exposing (..)
import Html.Attributes exposing (class, href)
import Html.Events exposing (onSubmit)
import Http
import Json.Encode as Encode exposing (Value)
import Route
import Session exposing (Session)
import Viewer exposing (Viewer)



-- MODEL


type alias Model =
    { form : Form
    , problems : List String
    , session : Session
    }


type alias Form =
    { username : FormField
    , password : FormField
    , isLoading : Bool
    }



-- ACCESSORS


toSession : Model -> Session
toSession model =
    model.session


updateSessionWith : Model -> Session -> Model
updateSessionWith model session =
    { model | session = session }



-- INIT


init : Session -> ( Model, Cmd msg )
init session =
    let
        username =
            FormField.config
                |> withLabel "Username"
                |> withType FormField.Text
                |> withConstraints [ NotEmpty, MinLength 4 ]

        password =
            FormField.config
                |> withLabel "Password"
                |> withType FormField.Password
                |> withConstraints [ NotEmpty, MinLength 8 ]

        form =
            { username = username, password = password, isLoading = False }
    in
    ( { form = form
      , problems = []
      , session = session
      }
    , Cmd.none
    )



-- VIEW


view : Model -> { title : String, content : Html Msg }
view model =
    { title = "Login"
    , content = loginView model
    }


loginView : Model -> Html Msg
loginView model =
    Grid.container [ Size.h100 ]
        [ Grid.row
            []
            [ Grid.col
                [ Col.xs12
                , Col.textAlign Text.alignXsCenter
                ]
                [ span [ class "text-uppercase display-3" ]
                    [ text "Login" ]
                ]
            , Grid.col
                [ Col.xs12
                , Col.textAlign Text.alignXsCenter
                ]
                [ span [ class "text-uppercase font-weight-lighter h2" ]
                    [ text "Welcome back" ]
                ]
            ]
        , cardView model
        ]


cardView : Model -> Html Msg
cardView model =
    Grid.row
        [ Row.middleXs
        , Row.centerXs
        , Row.attrs [ Size.h75 ]
        ]
        [ Grid.col
            [ Col.xs10
            , Col.md8
            , Col.lg6
            ]
            [ Card.config
                [ Card.align Text.alignXsCenter
                ]
                |> Card.block []
                    [ Block.custom <| errorView model.problems
                    , Block.custom <| formLogin model.form
                    ]
                |> Card.view
            ]
        ]


errorView : List String -> Html Msg
errorView problems =
    if List.isEmpty problems then
        span [] []

    else
        ul [ class "alert alert-danger" ] <|
            List.map (\error -> li [] [ text error ]) problems


formLogin : Form -> Html Msg
formLogin model =
    Html.form [ onSubmit SubmittedForm ]
        [ FormField.view EnteredUsername model.username
        , FormField.view EnteredPassword model.password
        , Button.button [ Button.primary, Button.disabled model.isLoading ] <|
            if model.isLoading then
                [ span [ class "spinner-border spinner-border-sm" ] [] ]

            else
                [ text "Login" ]
        ]



-- UPDATE


type Msg
    = EnteredUsername String
    | EnteredPassword String
    | SubmittedForm
    | CompletedLogin (Result Http.Error Viewer)
    | GotSession Session


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        EnteredUsername email ->
            let
                updateField =
                    \field ->
                        FormField.withValue email field
            in
            ( updateForm (\form -> { form | username = updateField form.username }) model
            , Cmd.none
            )

        EnteredPassword pwd ->
            let
                updateField =
                    \field ->
                        FormField.withValue pwd field
            in
            ( updateForm (\form -> { form | password = updateField form.password }) model
            , Cmd.none
            )

        SubmittedForm ->
            let
                validatedModel =
                    validate model
            in
            case validatedModel of
                Err updatedModel ->
                    ( updatedModel, Cmd.none )

                Ok updatedModel ->
                    ( { updatedModel
                        | form = updateLoading True updatedModel.form
                        , problems = []
                      }
                    , loginCmd updatedModel.form
                    )

        CompletedLogin (Ok viewer) ->
            ( { model | form = updateLoading False model.form }
            , Viewer.store viewer
            )

        CompletedLogin (Err error) ->
            let
                serverErrors =
                    Api.decodeErrors error
            in
            ( { model
                | problems = List.append model.problems serverErrors
                , form = updateLoading False model.form
              }
            , Cmd.none
            )

        GotSession session ->
            ( { model | session = session }
            , Route.replaceUrl (Session.navKey session) Route.Home
            )


updateForm : (Form -> Form) -> Model -> Model
updateForm transform model =
    { model | form = transform model.form }


updateLoading : Bool -> Form -> Form
updateLoading loading form =
    { form | isLoading = loading }



-- VALIDATE


validate : Model -> Result Model Model
validate model =
    let
        updatedUsername =
            FormField.validate model.form.username

        updatedPassword =
            FormField.validate model.form.password

        updatedModel =
            updateForm
                (\form ->
                    { form
                        | username = updatedUsername
                        , password = updatedPassword
                    }
                )
                model
    in
    if FormField.isFormInvalid [ updatedUsername, updatedPassword ] then
        Err updatedModel

    else
        Ok updatedModel



-- HTTP


loginCmd : Form -> Cmd Msg
loginCmd form =
    -- TODO: Let only ValidatedForm type
    let
        user =
            Encode.object
                [ ( "login", Encode.string <| FormField.toValue form.username )
                , ( "password", Encode.string <| FormField.toValue form.password )
                ]

        body =
            Encode.object [ ( "user", user ) ]
                |> Http.jsonBody
    in
    Api.login body Viewer.decoder CompletedLogin



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Session.changes GotSession (Session.bundle model.session)
