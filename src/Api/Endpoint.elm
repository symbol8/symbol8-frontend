module Api.Endpoint exposing (Endpoint, login, posts, request, user)

import Http
import Url.Builder exposing (QueryParameter)


{-| Http.request, except it takes an Endpoint instead of a Url.
-}
request :
    { method : String
    , url : Endpoint
    , expect : Http.Expect msg
    , headers : List Http.Header
    , body : Http.Body
    , timeout : Maybe Float
    , tracker : Maybe String
    }
    -> Cmd msg
request config =
    Http.request
        { method = config.method
        , url = unwrap config.url
        , expect = config.expect
        , headers = config.headers
        , body = config.body
        , timeout = config.timeout
        , tracker = config.tracker
        }



-- TYPES


{-| Url to the Symbol API

This is not exposed outside, it's an opaque type

-}
type Endpoint
    = Endpoint String


unwrap : Endpoint -> String
unwrap (Endpoint str) =
    str


url : List String -> List QueryParameter -> Endpoint
url paths queryParams =
    -- NOTE: Url.Builder takes care of percent-encoding special URL characters.
    -- See https://package.elm-lang.org/packages/elm/url/latest/Url#percentEncode
    Url.Builder.crossOrigin "https://api.symbol8.art/api"
        ("v1" :: paths)
        queryParams
        |> Endpoint



-- ENDPOINTS


login : Endpoint
login =
    url [ "sessions", "create" ] []


user : Int -> Endpoint
user id =
    url [ "users", String.fromInt id ] []



-- POSTS


posts : Endpoint
posts =
    url [ "posts" ] []
