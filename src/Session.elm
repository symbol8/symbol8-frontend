module Session exposing (Session, auth, bundle, changes, fromViewer, header, navKey, toBundle, viewer, withHeader)

import Api exposing (Auth)
import Browser.Navigation as Navigation exposing (Key)
import Page.Header as Header
import Viewer exposing (Viewer)



-- MODEL


type Session
    = LoggedIn Bundle Viewer
    | Guest Bundle


type alias Bundle =
    { key : Key
    , header : Header.Model
    }



-- ACCESSORS


viewer : Session -> Maybe Viewer
viewer session =
    case session of
        LoggedIn _ val ->
            Just val

        Guest _ ->
            Nothing


auth : Session -> Maybe Auth
auth session =
    case session of
        LoggedIn _ val ->
            Just (Viewer.auth val)

        Guest _ ->
            Nothing


bundle : Session -> Bundle
bundle session =
    case session of
        LoggedIn val _ ->
            val

        Guest val ->
            val


toBundle : Key -> Header.Model -> Bundle
toBundle keyVal headerVal =
    { key = keyVal
    , header = headerVal
    }


navKey : Session -> Key
navKey session =
    bundle session
        |> .key


header : Session -> Header.Model
header session =
    bundle session
        |> .header


withHeader : Header.Model -> Session -> Session
withHeader header_ session =
    let
        updatedHeader bundle_ =
            { bundle_ | header = header_ }
    in
    case session of
        LoggedIn bundle_ viewer_ ->
            LoggedIn (updatedHeader bundle_) viewer_

        Guest bundle_ ->
            Guest <| updatedHeader bundle_



-- CHANGES


changes : (Session -> msg) -> Bundle -> Sub msg
changes toMsg bundle_ =
    Api.viewerChanges
        (\maybeViewer ->
            toMsg <| fromViewer maybeViewer bundle_
        )
        Viewer.decoder


fromViewer : Maybe Viewer -> Bundle -> Session
fromViewer maybeViewer bundleVal =
    case maybeViewer of
        Just user ->
            LoggedIn bundleVal user

        Nothing ->
            Guest bundleVal
