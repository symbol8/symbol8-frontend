module Api exposing (Auth, application, decodeErrors, delete, get, login, post, put, storeAuthWith, username, viewerChanges)

import Api.Endpoint as Endpoint exposing (Endpoint)
import Avatar exposing (Avatar)
import Browser
import Browser.Navigation as Nav
import Http exposing (Body, Error)
import Json.Decode as Decode exposing (Decoder, Value, decodeString, field, string)
import Json.Encode as Encode exposing (Value)
import Logo exposing (Logo)
import Port
import Url exposing (Url)
import Username exposing (Username)



-- AUTH


{-| Auth system is used for the User, the logged-in user

This type includes:

  - Username type
  - Auth token (Bearer)

-}
type Auth
    = Auth Username String


username : Auth -> Username
username (Auth user _) =
    user


authHeader : Auth -> Http.Header
authHeader (Auth _ token) =
    Http.header "Authorization" ("Token " ++ token)


{-| It's important that this is never exposed!

We epxose `login` and `application` instead, so we can be certain that if anyone
ever has access to a `Cred` value, it came from either the login API endpoint
or was passed in via flags.

-}
authDecoder : Decoder Auth
authDecoder =
    Decode.map2 Auth
        (field "username" Username.decoder)
        (field "token" string)


decoderFromAuth : Decoder (Auth -> a) -> Decoder a
decoderFromAuth decoder =
    Decode.map2 (\fromAuth auth -> fromAuth auth)
        decoder
        authDecoder



-- PERSISTENCE


decode : Decoder (Auth -> viewer) -> Value -> Result Decode.Error viewer
decode decoder value =
    -- localStorage give to us a JSON String;
    -- first decode the Value to String
    Decode.decodeValue Decode.string value
        |> Result.andThen (\str -> Decode.decodeString (Decode.field "user" (decoderFromAuth decoder)) str)


viewerChanges : (Maybe viewer -> msg) -> Decoder (Auth -> viewer) -> Sub msg
viewerChanges toMsg decoder =
    Port.onStoreChange (\value -> toMsg (decodeFromChange decoder value))


decodeFromChange : Decoder (Auth -> viewer) -> Value -> Maybe viewer
decodeFromChange viewerDecoder val =
    Decode.decodeValue (storageDecoder viewerDecoder) val
        |> Result.toMaybe


storeAuthWith : Auth -> Avatar -> Cmd msg
storeAuthWith (Auth username_ token) avatar =
    let
        json =
            Encode.object
                [ ( "user"
                  , Encode.object
                        [ ( "username", Username.encode username_ )
                        , ( "token", Encode.string token )
                        , ( "image", Avatar.encode avatar )
                        ]
                  )
                ]
    in
    Port.storeCache (Just json)


logout : Cmd msg
logout =
    Port.storeCache Nothing



-- SERIALIZATION


application :
    Decoder (Auth -> viewer)
    ->
        { init : Logo -> Maybe viewer -> Url -> Nav.Key -> ( model, Cmd msg )
        , onUrlChange : Url -> msg
        , onUrlRequest : Browser.UrlRequest -> msg
        , subscriptions : model -> Sub msg
        , update : msg -> model -> ( model, Cmd msg )
        , view : model -> Browser.Document msg
        }
    -> Program Value model msg
application viewerDecoder config =
    let
        init flags url navKey =
            let
                maybeViewer =
                    Decode.decodeValue Decode.string flags
                        |> Result.andThen (Decode.decodeString (storageDecoder viewerDecoder))
                        |> Result.toMaybe

                logo =
                    Decode.decodeValue Decode.string flags
                        |> Result.withDefault ""
                        |> Decode.decodeString Logo.decoder
                        |> Result.withDefault Logo.empty
            in
            config.init logo maybeViewer url navKey
    in
    Browser.application
        { init = init
        , onUrlChange = config.onUrlChange
        , onUrlRequest = config.onUrlRequest
        , subscriptions = config.subscriptions
        , update = config.update
        , view = config.view
        }


storageDecoder : Decoder (Auth -> viewer) -> Decoder viewer
storageDecoder viewerDecoder =
    Decode.field "user" (decoderFromAuth viewerDecoder)



-- HTTP


{-| Requires to map the command

The new api of http has changed and now Expect wants a msg with Result

-}
get : Endpoint -> Maybe Auth -> Decoder a -> (Result Error a -> msg) -> Cmd msg
get url maybeAuth decoder toMsg =
    Endpoint.request
        { method = "GET"
        , url = url
        , expect = Http.expectJson toMsg decoder
        , headers =
            case maybeAuth of
                Just auth ->
                    [ authHeader auth ]

                Nothing ->
                    []
        , body = Http.emptyBody
        , timeout = Nothing
        , tracker = Nothing
        }


post : Endpoint -> Maybe Auth -> Body -> Decoder a -> (Result Error a -> msg) -> Cmd msg
post url maybeAuth body decoder toMsg =
    Endpoint.request
        { method = "POST"
        , url = url
        , expect = Http.expectJson toMsg decoder
        , headers =
            case maybeAuth of
                Just auth ->
                    [ authHeader auth ]

                Nothing ->
                    []
        , body = body
        , timeout = Nothing
        , tracker = Nothing
        }


put : Endpoint -> Auth -> Body -> Decoder a -> (Result Error a -> msg) -> Cmd msg
put url auth body decoder toMsg =
    Endpoint.request
        { method = "POST"
        , url = url
        , expect = Http.expectJson toMsg decoder
        , headers =
            [ authHeader auth ]
        , body = body
        , timeout = Nothing
        , tracker = Nothing
        }


delete : Endpoint -> Auth -> Body -> Decoder a -> (Result Error a -> msg) -> Cmd msg
delete url auth body decoder toMsg =
    Endpoint.request
        { method = "DELETE"
        , url = url
        , expect = Http.expectJson toMsg decoder
        , headers =
            [ authHeader auth ]
        , body = body
        , timeout = Nothing
        , tracker = Nothing
        }


{-| Send the login http request to API

The decoder is used to transform the returning json into the desidered object.

toMsg - is an type parameter which should contain the Http Result and it's called inside the update when the response has come

-}
login : Body -> Decoder (Auth -> a) -> (Result Error a -> msg) -> Cmd msg
login body decoder toMsg =
    post Endpoint.login Nothing body (Decode.field "user" (decodeFromAuth decoder)) toMsg


decodeFromAuth : Decoder (Auth -> a) -> Decoder a
decodeFromAuth decoder =
    Decode.map2 (\fromAuth auth -> fromAuth auth)
        decoder
        authDecoder



-- ERRORS


decodeErrors : Http.Error -> List String
decodeErrors error =
    case error of
        Http.BadUrl url ->
            List.singleton <| "Bad url: " ++ url

        Http.Timeout ->
            List.singleton <| "Did not get a response in time"

        Http.NetworkError ->
            List.singleton <| "There seems to be an error with the internet connection"

        Http.BadStatus code ->
            List.singleton <| "The server responded with this status code: " ++ String.fromInt code

        Http.BadBody reason ->
            List.singleton <| "A bad body was returned: " ++ reason
