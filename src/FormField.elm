module FormField exposing (Constraint(..), FormField, FormType(..), config, hasErrors, isFormInvalid, toValue, validate, view, withConstraints, withLabel, withType, withValue)

import Bootstrap.Form as Form
import Bootstrap.Form.Input as Input exposing (Option)
import Html exposing (Html, li, text, ul)
import Html.Attributes exposing (class, for)



-- MODEL


type FormField
    = FormField FormFieldRec


type alias FormFieldRec =
    { id : String
    , label : String
    , value : String
    , kind : FormType
    , constraints : List Constraint
    , errors : Maybe (List String)
    }


type FormType
    = Email
    | Password
    | Text


type Constraint
    = NotEmpty
    | MinLength Int



-- ACCESSORS


config : FormField
config =
    FormField
        { id = ""
        , label = ""
        , value = ""
        , kind = Text
        , constraints = []
        , errors = Nothing
        }



-- OPTIONS


withLabel : String -> FormField -> FormField
withLabel label form_ =
    updateFormField (\form -> { form | label = label }) form_


withValue : String -> FormField -> FormField
withValue value form_ =
    updateFormField (\form -> { form | value = value }) form_


withType : FormType -> FormField -> FormField
withType kind form_ =
    updateFormField (\form -> { form | kind = kind }) form_


withConstraints : List Constraint -> FormField -> FormField
withConstraints constraints form_ =
    updateFormField (\form -> { form | constraints = constraints }) form_


withErrors : Maybe (List String) -> FormField -> FormField
withErrors errors form_ =
    updateFormField (\form -> { form | errors = errors }) form_


updateFormField :
    (FormFieldRec -> FormFieldRec)
    -> FormField
    -> FormField
updateFormField mapper (FormField form_) =
    FormField <| mapper form_


hasErrors : FormField -> Bool
hasErrors (FormField form) =
    case form.errors of
        Just _ ->
            True

        Nothing ->
            False


toValue : FormField -> String
toValue (FormField form) =
    form.value



-- VIEW


view : (String -> msg) -> FormField -> Html msg
view msg (FormField form) =
    let
        maybeErrors =
            List.map (\err -> li [ class "text-left" ] [ text err ]) <|
                Maybe.withDefault [] form.errors
    in
    Form.group [] <|
        [ Form.label [ for form.id ] [ text form.label ]
        , formKindView form.kind
            [ Input.id form.id
            , Input.onInput msg
            ]
        , Form.invalidFeedback [ class "d-block" ]
            [ ul [ class "list-unstyled" ] <|
                maybeErrors
            ]
        ]


formKindView : FormType -> List (Option msg) -> Html msg
formKindView kind options =
    case kind of
        Email ->
            Input.email options

        Password ->
            Input.password options

        Text ->
            Input.text options



-- VALIDATION


isFormInvalid : List FormField -> Bool
isFormInvalid formList =
    List.any (\field -> hasErrors field) formList


validate : FormField -> FormField
validate (FormField form) =
    let
        unvalidatedForm =
            cleanErrors form

        updatedErrors =
            List.concat <|
                List.map
                    (\constraint -> checkConstraint constraint unvalidatedForm)
                    form.constraints

        toRecord =
            \maybeErrors ->
                { form | errors = maybeErrors }
    in
    if List.isEmpty updatedErrors then
        FormField <| toRecord Nothing

    else
        FormField <| toRecord <| Just updatedErrors


cleanErrors : FormFieldRec -> FormFieldRec
cleanErrors form =
    { form | errors = Nothing }


kindToString : FormType -> String
kindToString kind =
    case kind of
        Email ->
            "email"

        Password ->
            "password"

        Text ->
            "text"


checkConstraint : Constraint -> FormFieldRec -> List String
checkConstraint constraint form =
    let
        value =
            form.value

        kind =
            kindToString form.kind

        appendError =
            \error ->
                List.append (Maybe.withDefault [] form.errors) error
    in
    case constraint of
        NotEmpty ->
            let
                error =
                    [ "The field cannot be empty" ]
            in
            if String.isEmpty value then
                appendError error

            else
                []

        MinLength atLeast ->
            let
                errorValue =
                    String.fromInt atLeast

                error =
                    [ "The " ++ kind ++ " should be at least " ++ errorValue ++ " characters long" ]
            in
            if String.length value < atLeast then
                appendError error

            else
                []
