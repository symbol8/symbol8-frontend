port module Port exposing (onStoreChange, storeCache)

import Json.Encode as Encode exposing (Value)



-- PORTS


port onStoreChange : (Value -> msg) -> Sub msg


port storeCache : Maybe Value -> Cmd msg
