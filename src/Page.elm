module Page exposing (mapContent, view)

import Bootstrap.Utilities.Size as Size
import Browser exposing (Document)
import Html exposing (Html, div)
import Html.Attributes exposing (style)



-- VIEW


view : Html msg -> { title : String, content : Html msg } -> Document msg
view header { title, content } =
    let
        paddedContent =
            div
                [ Size.h100
                , style "paddingTop" "88px"
                ]
                [ content ]
    in
    { title = compositeTitle title
    , body = header :: [ paddedContent ]
    }


compositeTitle : String -> String
compositeTitle title =
    if String.isEmpty title then
        "Symbol8"

    else
        title ++ " | Symbol8"



-- MAPPER


mapContent : (a -> msg) -> { title : String, content : Html a } -> { title : String, content : Html msg }
mapContent toMsg oldContent =
    let
        newContent =
            Html.map toMsg oldContent.content
    in
    { title = oldContent.title, content = newContent }
