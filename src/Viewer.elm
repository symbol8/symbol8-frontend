module Viewer exposing (Viewer, auth, avatar, decoder, store, username)

import Api exposing (Auth)
import Avatar exposing (Avatar)
import Json.Decode as Decode exposing (Decoder)
import Username exposing (Username)



-- MODEL


type Viewer
    = Viewer Avatar Auth



-- ACCESSORS


auth : Viewer -> Auth
auth (Viewer _ token) =
    token


username : Viewer -> Username
username (Viewer _ token) =
    Api.username token


avatar : Viewer -> Avatar
avatar (Viewer avatar_ _) =
    avatar_



-- SERIALIZATION


decoder : Decoder (Auth -> Viewer)
decoder =
    Decode.map Viewer <|
        Decode.field "image" Avatar.decoder


store : Viewer -> Cmd msg
store (Viewer avatarVal authVal) =
    Api.storeAuthWith
        authVal
        avatarVal
