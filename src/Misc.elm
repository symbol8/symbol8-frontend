module Misc exposing (Logo, logo, logoSrc)

import Html exposing (Attribute)
import Html.Attributes exposing (alt, src)



-- MODEL


type Logo
    = Logo String



-- PUBLIC CONSTRUCTORS


logo : String -> Logo
logo path =
    Logo path


logoSrc : Logo -> Attribute msg
logoSrc (Logo path) =
    if String.isEmpty path then
        alt "Logo"

    else
        src path
