import './assets/stylesheets/main.css';
import logoPath from './assets/images/logo.svg';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import 'jquery/dist/jquery.slim.js';
import 'font-awesome/css/font-awesome.min.css';
import { Elm } from './Main.elm';
import registerServiceWorker from './registerServiceWorker';

var storageKey = "store";

function main() {
  let app = Elm.Main.init({
    node: document.getElementById('root'),
    flags: initJson()
  });
  storagePort(app);
}

function initJson() {
  let store = getStore();

  let json = `{
    "logoPath": "${logoPath}",
    ${store}
  }`
  console.log(json);
  return json;
}

function getStore() {
  let store = localStorage.getItem(storageKey);

  if (store === null) {
    store = `"user": "null"`
  }

  return store;
}

function storagePort(app) {
    app.ports.storeCache.subscribe((val) => {
        if (val === null) {
            localStorage.removeItem(storageKey)
        } else {
            localStorage.setItem(storageKey, JSON.stringify(val))
        }

        //  Let know Elm that the session was stored
        app.ports.onStoreChange.send(val);
    });

    window.addEventListener("storage", (event) => {
        if (event.storageArea === localStorage && event.key === storageKey) {
            app.ports.onStoreChange.send(event.newValue)
        }
    }, false);
}

main();

registerServiceWorker();
