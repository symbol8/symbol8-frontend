module Avatar exposing (Avatar, decoder, encode, src, toMaybeString)

import Html exposing (Attribute)
import Html.Attributes as Attr
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode exposing (Value)



-- MODEL


type Avatar
    = Avatar (Maybe String)



-- SERIALIZATION


decoder : Decoder Avatar
decoder =
    Decode.map Avatar (Decode.nullable Decode.string)


encode : Avatar -> Value
encode (Avatar maybeUrl) =
    case maybeUrl of
        Just url ->
            Encode.string url

        Nothing ->
            Encode.null


src : Avatar -> Attribute msg
src (Avatar maybeUrl) =
    case maybeUrl of
        Nothing ->
            -- TODO: Personalize the avatar
            Attr.src "https://symbol8.art/default_avatar.png"

        Just "" ->
            -- TODO: Personalize the avatar
            Attr.src "https://symbol8.art/default_avatar.png"

        Just url ->
            Attr.src url


toMaybeString : Avatar -> Maybe String
toMaybeString (Avatar maybeUrl) =
    maybeUrl
