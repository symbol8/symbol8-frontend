module Main exposing (main)

import Api
import Browser
import Browser.Navigation as Nav
import Html
import Json.Decode exposing (Value)
import Logo exposing (Logo)
import Page
import Page.Blank as Blank
import Page.Header as Header
import Page.Home as Home
import Page.Login as Login
import Page.NotFound as NotFound
import Page.Philosophy as Philosophy
import Page.Post as Post
import Route exposing (Route)
import Session exposing (Session)
import Url exposing (Url)
import Viewer exposing (Viewer)



-- MODEL


type Model
    = Redirect Session
    | NotFound Session
    | Home Session
    | Philosophy Session
    | Post Post.Model
    | Login Login.Model



-- INIT


init : Logo -> Maybe Viewer -> Url -> Nav.Key -> ( Model, Cmd Msg )
init logo maybeViewer url key =
    let
        ( headerModel, headerCmd ) =
            Header.init logo

        mappedCmd =
            Cmd.map GotHeaderMsg headerCmd

        ( model, cmd ) =
            changeRouteTo (Route.fromUrl url) <|
                Redirect
                    (Session.toBundle key headerModel
                        |> Session.fromViewer maybeViewer
                    )
    in
    ( model
    , Cmd.batch [ mappedCmd, cmd ]
    )



-- UPDATE


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url
    | GotHeaderMsg Header.Msg
    | GotLoginMsg Login.Msg
    | GotPostMsg Post.Msg
    | GotSession Session


toSession : Model -> Session
toSession page =
    case page of
        Redirect session ->
            session

        NotFound session ->
            session

        Home session ->
            session

        Philosophy session ->
            session

        Post session ->
            Post.toSession session

        Login login ->
            Login.toSession login


changeRouteTo : Route -> Model -> ( Model, Cmd Msg )
changeRouteTo route model =
    let
        session =
            toSession model
    in
    case route of
        Route.NotFound ->
            ( NotFound session, Cmd.none )

        Route.Home ->
            ( Home session, Cmd.none )

        Route.Philosophy ->
            ( Philosophy session, Cmd.none )

        Route.Post page ->
            let
                ( subModel, cmd ) =
                    Post.init session page
            in
            ( Post subModel
            , Cmd.map GotPostMsg cmd
            )

        Route.Login ->
            Login.init session
                |> updateWith Login GotLoginMsg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        session =
            toSession model
    in
    case ( msg, model ) of
        ( LinkClicked urlRequest, _ ) ->
            case urlRequest of
                Browser.Internal url ->
                    case url.fragment of
                        Nothing ->
                            ( model, Cmd.none )

                        Just _ ->
                            ( model
                            , Nav.pushUrl (Session.navKey session) (Url.toString url)
                            )

                Browser.External href ->
                    ( model
                    , Nav.load href
                    )

        ( UrlChanged url, _ ) ->
            changeRouteTo (Route.fromUrl url) model

        ( GotLoginMsg subMsg, Login login ) ->
            Login.update subMsg login
                |> updateWith Login GotLoginMsg

        ( GotPostMsg subMsg, Post post ) ->
            Post.update subMsg post
                |> updateWith Post GotPostMsg

        ( GotSession newSession, Redirect _ ) ->
            ( Redirect session
            , Route.replaceUrl (Session.navKey newSession) Route.Home
            )

        ( GotHeaderMsg headerMsg, NotFound _ ) ->
            (\s -> s)
                |> updateHeaderWith NotFound session headerMsg

        ( GotHeaderMsg headerMsg, Home _ ) ->
            (\s -> s)
                |> updateHeaderWith Home session headerMsg

        ( GotHeaderMsg headerMsg, Philosophy _ ) ->
            (\s -> s)
                |> updateHeaderWith Philosophy session headerMsg

        ( GotHeaderMsg headerMsg, Post post ) ->
            Post.updateSessionWith post
                |> updateHeaderWith Post session headerMsg

        ( GotHeaderMsg headerMsg, Login login ) ->
            Login.updateSessionWith login
                |> updateHeaderWith Login session headerMsg

        ( _, _ ) ->
            -- Discart messaged camed from the wrong page
            ( model, Cmd.none )


updateHeaderWith :
    (subModel -> Model)
    -> Session
    -> Header.Msg
    -> (Session -> subModel)
    -> ( Model, Cmd Msg )
updateHeaderWith toModel session headerMsg toSubModel =
    let
        ( updatedHeader, updatedCmd ) =
            Session.header session
                |> Header.update headerMsg

        updatedSubModel =
            Session.withHeader updatedHeader session
                |> toSubModel
    in
    ( toModel updatedSubModel
    , Cmd.map GotHeaderMsg updatedCmd
    )


updateWith :
    (subModel -> Model)
    -> (subMsg -> Msg)
    -> ( subModel, Cmd subMsg )
    -> ( Model, Cmd Msg )
updateWith toModel toMsg ( subModel, subMsg ) =
    ( toModel subModel
    , Cmd.map toMsg subMsg
    )



-- VIEW


view : Model -> Browser.Document Msg
view model =
    let
        header =
            \page ->
                Html.map GotHeaderMsg (Header.view (Session.header (toSession model)) page)

        viewPage =
            \page content ->
                Page.view (header page) content

        viewPageMsg =
            \page toMsg content ->
                let
                    updatedContent =
                        Page.mapContent toMsg content

                    { title, body } =
                        viewPage page updatedContent
                in
                { title = title
                , body = body
                }
    in
    case model of
        Redirect _ ->
            viewPage Header.Other Blank.view

        NotFound _ ->
            viewPage Header.Other NotFound.view

        Home _ ->
            let
                logo =
                    toSession model
                        |> Session.header
                        |> Header.toLogo
            in
            viewPage Header.Home (Home.view logo)

        Philosophy _ ->
            viewPage Header.Philosophy Philosophy.view

        Post post ->
            viewPageMsg Header.Posts GotPostMsg (Post.view post)

        Login login ->
            viewPageMsg Header.Login GotLoginMsg (Login.view login)



-- SUBSCRIPTION


subscriptions : Model -> Sub Msg
subscriptions model =
    let
        headerSub =
            Sub.map GotHeaderMsg
                (Header.subscriptions <| Session.header (toSession model))

        appendSub sub =
            Sub.batch <| sub :: [ headerSub ]
    in
    case model of
        Login login ->
            Sub.map GotLoginMsg (Login.subscriptions login)
                |> appendSub

        Post post ->
            Sub.map GotPostMsg (Post.subscriptions post)
                |> appendSub

        Redirect _ ->
            Session.bundle (toSession model)
                |> Session.changes GotSession
                |> appendSub

        _ ->
            appendSub Sub.none



-- MAIN


main : Program Value Model Msg
main =
    Api.application Viewer.decoder
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = LinkClicked
        , onUrlChange = UrlChanged
        }
