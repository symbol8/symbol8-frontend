module Route exposing (PostPage(..), Route(..), fromUrl, href, replaceUrl)

import Browser.Navigation as Navigation
import Html exposing (Attribute)
import Html.Attributes as Attr
import Url exposing (Url)
import Url.Parser as Parser exposing ((</>), Parser, custom, int, map, oneOf, s, top)



-- MODEL


type Route
    = NotFound
    | Home
    | Philosophy
    | Post PostPage
    | Login


type PostPage
    = RecentPosts
    | AllPosts
    | ShowPost Int
    | NewPost
    | EditPost Int



-- ROUTER


urlParser : Parser (Route -> a) a
urlParser =
    oneOf
        [ map Home top
        , map Philosophy (s "philosophy")
        , map Post (s "posts" </> postUrlParser)
        , map Login (s "login")
        ]


postUrlParser : Parser (PostPage -> c) c
postUrlParser =
    oneOf
        [ map AllPosts (s "all")
        , map RecentPosts (s "recent")
        , map ShowPost int
        , map NewPost (s "new")
        , map EditPost (s "edit" </> int)
        ]


postRouteToString : PostPage -> String
postRouteToString postRoute =
    case postRoute of
        AllPosts ->
            "all"

        RecentPosts ->
            "recent"

        ShowPost num ->
            String.fromInt num

        NewPost ->
            "new"

        EditPost num ->
            "edit/" ++ String.fromInt num



-- PUBLIC HELPERS


href : Route -> Attribute msg
href targetRoute =
    Attr.href (routeToString targetRoute)


replaceUrl : Navigation.Key -> Route -> Cmd msg
replaceUrl key route =
    Navigation.replaceUrl key (routeToString route)


fromUrl : Url -> Route
fromUrl url =
    { url | path = Maybe.withDefault "" url.fragment, fragment = Nothing }
        |> Parser.parse urlParser
        |> Maybe.withDefault NotFound



-- INTERNAL


routeToString : Route -> String
routeToString page =
    let
        pieces =
            case page of
                Home ->
                    []

                Philosophy ->
                    [ "philosophy" ]

                Post action ->
                    [ "posts", postRouteToString action ]

                Login ->
                    [ "login" ]

                NotFound ->
                    []
    in
    "#/" ++ String.join "/" pieces
