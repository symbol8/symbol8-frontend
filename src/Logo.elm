module Logo exposing (Logo, decoder, empty, src)

import Html exposing (Attribute)
import Html.Attributes as Attr
import Json.Decode as Decode exposing (Decoder)


type Logo
    = Logo String



-- DECODER


decoder : Decoder Logo
decoder =
    Decode.map (\path -> Logo path) Decode.string
        |> Decode.field "logoPath"



-- HELPERS


empty : Logo
empty =
    Logo ""


src : Logo -> Attribute msg
src (Logo url) =
    Attr.src url
